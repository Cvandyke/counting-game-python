###############################################################################
#
# Filename: theCountingGame.py
#
# CS Assignment: Programming Project 2
# Author: Chandler Van Dyke
# Date: 10/23/2016
# Section: CST215 MWF 9:50
#
###############################################################################
#
# Algorithm
# Varibales:
#   g = goal value = 100
#   s = game value / starting value = 0
#   cmin = minimum choice > 0
#   cmax = max choice = 10
#   turn num = 0 (incremented by turn)
#   player turn = 1 or 0
#   best options = [12,23,34,45,56,67,78,89]
#
# 1. print game rules
# 2. get starting values
# 3. while s < g:
#   a. player turn = turn num % 2
#   b. if player turn = 0 (player 1 turn)
#       1. get player turn
#       2. s += player turn
#       3. winner = player1
#   c. if player turn = 1 (player 2 turn)
#       1. get player turn
#       2. s += player turn
#       3. winner = player1
#   d. turn num += 1
# 4. print winner
# 5. print strategy
#
###############################################################################
# for funsies/ computer "thinking"
import time
# for computer choice
import random


###VARIABLES###
goal = int  # goal value
goalStr = "Goal Value"
start = int  # game value
startStr = "Start Value"
cmin = int  # minimum choice
cminStr = "Minimum Choice (must be greater than 0)"
cmax = int  # maximum choice
cmaxStr = "Maximum Choice (must be greater than cmin)"
playerInput = str
playerNum = int  # player choice
computerNum = int  # computer choice
shouldWin = "Undecided"  # for determining who should winb
gameType = int
winningOptions = []
winner = str  # who won


###FUNCTIONS###
# get an int value without errors
def getIntVal (assStr = str):
    choosingNum = True
    numberStr = str
    number = int
    while choosingNum:
        # while player is choosing an int
        try:
            numberStr = input("Please enter a number for {}: "
                               .format(assStr))
            # try to convert to int
            number = int(numberStr)
            # it was an int
            choosingNum = False
            print("\n")
        except ValueError:
            # it was not an int
            print("{} is not of type int, please enter an integer.\n"
                  .format(numberStr))
    return number



# starting stuff
def startStuff ():
    # Intro
    # Objective
    # Rules
    # Game board rules
    print("Welcome to the COUNTING GAME!!!\n")
    print("Objective: The player who reaches the goal value (or greater) \n"
          "first is the winner.\n")
    print("Rules: The first player starts at the START value defined by the \n"
          "user. They may add any number in a range of numbers defined also \n"
          "by the user (cmin - cmax). Cmin must be less than Cmax for there \n"
          "to be strategy involved. The next player builds on the first \n"
          "player's number by choosing a number between the bounds defined \n"
          "earlier. The game continues, each player taking their turn until \n"
          "the goal value is reached. The player reaching the goal value \n"
          "first wins!\n")
    print("Game Board Example:\n")
    print("Start: ==> starting number for player turn")
    print("##Num: ==> player thats guessing's number")
    print("PWSFW: ==> Player Who Should Force Win\n")
    print("Reccomendations:")
    print("Start = 0")
    print("Goal = 100")
    print("Minimum Choice = 1")
    print("Maximum Choice = 10\n\n")


# end stuff
def endStuff(listOptions = list, player = str ):
    # Who won
    print("\n\n{} WINS!!!!\n".format(player))
    # Strategy
    print("Strategy: This game allows for a person to force a win starting \n"
          "early in the game.  In order to find the 'force win' options \n"
          "we must start at the goal value. If we look at it logically we \n"
          "want to give the other player a number just out of range of \n"
          "winning so that when the other player picks a number, we are able\n"
          "to pick the goal number.  We can backtrack to the start value \n"
          "using the same method. This algorithm includes that for every \n"
          "bad cmax values, there are cmin good options in a range cmin + \n"
          "cmax. For example if goal = 100, start = 0, cmin = 1, and \n"
          "cmax = 10. If we want to force a win, we need to count back \n"
          "10 + 1 (1 out of range) so that no matter what value they give \n"
          "back, we are able to win.  In this case the goal is to get to 89 \n"
          "to force the win. If we apply the same thing to the number 89, \n"
          "we want to get to 78, then 67, 56, 45, 34, 23, 12, and 1. \n"
          "Whoever gets to a number in that list first, is able to force a \n"
          "win. Another example: goal = 20, start = 0, cmin = 5, cmax = 7.\n"
          "Looking at this case, for every bad cmax values, there are cmin \n"
          "good values.  so starting at 20 we want to subtract 7+1 or 8 to \n"
          "get to our first force win option 12.  Then since we have more \n"
          "than 1 for our cmin, we need to get a range of 5 numbers for \n"
          "the force win option. So we get 12, 11, 10, 9, and 8 as winning \n"
          "options.  Then subtract cmax again and we get 1 as a winning \n"
          "option aswell.\n\n")
    # Show winning options
    print("The winning options in your game are as follows:")
    print(listOptions)


# find winning options / strategy
def getWinOptions(cmax = int, cmin = int, goal = int, start = int,):
    # initialize list
    wo = []
    # if no more options are viable
    if goal - cmax <= start:
        return wo

    else:
        # add bottom of previous range to list
        wo = []
        # get top of range
        goal -= cmax
        # add numbers in new range
        for option in range(goal - cmin, goal):
            if option >= cmin:
                wo.append(option)
        # decrement goal value to bottom of range
        goal -= cmin
        # get next range
        gwo = getWinOptions(cmax, cmin, goal, start)
        return wo + gwo


# get player turn
def playerTurn ():
    # player is choosing
    choosing = True
    while choosing:
        try:
            # get int
            # if int is not recived -> ask for int
            playerInput = input("P1Num: ")
            playerNum = int(playerInput)
        # not int
        except ValueError:
            print("\n{} is not an integer."
                  "\nPlease enter an integer.\n".format(playerInput))
            continue

        # VALID CHOICE
        if cmin <= playerNum <= cmax:

            choosing = False
            continue

        # if player choice is out of bounds (less than 0)
        # loop to ask again
        elif playerNum < cmin:
            print("\n{} is less than {}.\n"
                  "Please choose a number in range {}-{}.\n"
                  .format(playerNum, cmin, cmin, cmax))

        # if player choice is out of bounds (greater than 10)
        # loop to ask again
        else:
            print("\n{} is more than {}.\n"
                  "Please choose a number in range {}-{}.\n"
                  .format(playerNum, cmax, cmin, cmax))
    # return player number
    return playerNum


# get computer turn
def computerTurn (start, which):
    compWin = False

    for winningValue in winningOptions:
        # if winning value within range of cmin and cmax
        if cmin <= winningValue - start <= cmax:
            # choose winning value
            computerNum = winningValue - start
            compWin = True
            # no need to continue checking winning options
            break

    # if winning option is not within range
    if not compWin:
        # choose random number between 1 and 10 and add s
        computerNum  = random.randint(cmin,cmax)

    # computer is thinking
    time.sleep(.25)

    # show computer number
    print("C{}Num: {}".format(which,computerNum))
    return computerNum


# player vs computer
def playerVScomputer (start, goal, cmin, cmax):
    turn = 0
    turnNum = 0
    shouldWin = "UNDECIDED"
    # while game is not done
    while start < goal:
        # determine whos turn (0 -> player, 1 -> computer)
        turn = turnNum % 2
        # print starting number
        print("Start: {}".format(start))

        # player turn
        if turn == 0:
            # get player turn
            playerNum = playerTurn()
            # check if player turn is winning number
            # is winning number -> player should win
            if playerNum + start in winningOptions:
                shouldWin = "PLAYER"
            # is not winning number -> undecided or computer should win
            elif shouldWin == "PLAYER":
                shouldWin = "UNDECIDED"
            # add player turn to start value
            start += playerNum
            # update winner (Last play will win)
            winner = "Player Wins"

        # computer turn
        elif turn == 1:
            # get computer number
            computerNum = computerTurn(start, 1)
            # check if computer number is winning number
            # is winning number -> computer should win
            if computerNum + start in winningOptions:
                shouldWin = "COMPUTER"
            # add computer turn to start value
            start += computerNum
            # update winner (last play will win)
            winner = "Computer Wins"

        # print who should win
        print("PWSFW: {}".format(shouldWin))
        # spaceing
        print("\n")
        # increase turn number
        turnNum += 1
    return winner


# computer vs computer
def computerVScomputer(start, goal, cmin, cmax):
    turn = 0
    turnNum = 0
    shouldWin = "Undecided"
    computerPlayer = 0
    while start < goal:
        # determine who's turn (0 -> computer 1, 1 -> computer 2)
        turn = turnNum % 2
        computerPlayer = turn + 1

        # print starting number
        print("Start: {}".format(start))
        # get computer number
        computerNum = computerTurn(start, computerPlayer)
        # check if computer number is winning number

        # is winning number -> computer __ should win
        if computerNum + start in winningOptions:
            shouldWin = "COMPUTER {}".format(computerPlayer)

        # add computer turn to start value
        start += computerNum

        # update winner (last play will win)
        winner = "Computer {} Wins".format(computerPlayer)

        # print who should win
        print("PWSFW: {}".format(shouldWin))

        # spaceing
        print("\n")
        # increase turn number
        turnNum += 1
    return winner


###GAME START###
# INITIAL STUFF
startStuff()
start = getIntVal(startStr)
gettingGoal = True
while gettingGoal:
    # get gaol value
    goal = getIntVal(goalStr)
    # goal must be bigger than 1 (cmin)
    if goal > start:
        gettingGoal = False
    # error handeling
    else:
        print("{} is less than or equal to the start value {}"
              .format(goal, start))
        print("Please enter a number greater than {}\n".format(start))


#GET VALUES
# get min
gettingMin = True
while gettingMin:
    # get min value
    cmin = getIntVal(cminStr)
    # min must be greater than 0
    if cmin > 0:
        gettingMin = False
    # error handeling
    else:
        print("{} is less than 1".format(cmin))
        print("Please choose a number equal to or greater than 1\n")


#get max   
gettingMax = True
while gettingMax:
    # get max value
    cmax = getIntVal(cmaxStr)
    # max must be larger than min
    if cmax > cmin:
        gettingMax = False
    # error handeling
    else:
        print("{} is less than or equal to {}".format(cmax, cmin))
        print("Please enter a number greater than {}(cmin)\n".format(cmin))


# get winning options
winningOptions = getWinOptions(cmax, cmin, goal, start)
winningOptions.append(goal)
# sort winning options for explination
winningOptions.sort()


# GET GAME TYPE
gettingGame = True
print("What game type would you like?")
print("Enter '1' for Player VS Computer")
print("Enter '2' for Computer VS Computer")
while gettingGame:
    gameType = getIntVal("game type")
    # if valid gametype
    if gameType == 1 or gameType == 2:
        gettingGame = False

    # not valid game type error
    else:
        print("{} is not a valid game type.\n".format(gameType))
        print("What game type would you like?")
        print("Enter '1' for Player VS Computer")
        print("Enter '2' for Computer VS Computer")


# PLAY GAME
# Player vs computer
if gameType == 1:
    winner = playerVScomputer(start, goal, cmin, cmax)
# Computer vs computer
elif gameType == 2:
    winner = computerVScomputer(start, goal, cmin, cmax)

#end stuff
endStuff(winningOptions, winner)
