THE COUNTING GAME

Objective: The player who reaches the goal value (or greater) first wins.

Rules: The first player starts at the START value defined by the 
          user. They may add any number in a range of numbers defined also 
          by the user (cmin - cmax). Cmin must be less than Cmax for there 
          to be strategy involved. The next player builds on the first 
          player's number by choosing a number between the bounds defined 
          earlier. The game continues, each player taking their turn until 
          the goal value is reached. The player reaching the goal value 
          first wins!

Game Board example:

    Start: ==> starting number for player turn
    ##Num: ==> player thats guessing's number
    PWSFW: ==> Player Who Should Force Win

Winning Strategy:

	  This game allows for a person to force a win starting
          early in the game.  In order to find the 'force win' options 
          we must start at the goal value. If we look at it logically we 
          want to give the other player a number just out of range of 
          winning so that when the other player picks a number, we are able
          to pick the goal number.  We can backtrack to the start value 
          using the same method. This algorithm includes that for every 
          bad cmax values, there are cmin good options in a range cmin + 
          cmax. For example if goal = 100, start = 0, cmin = 1, and 
          cmax = 10. If we want to force a win, we need to count back 
          10 + 1 (1 out of range) so that no matter what value they give 
          back, we are able to win.  In this case the goal is to get to 89 
          to force the win. If we apply the same thing to the number 89, 
          we want to get to 78, then 67, 56, 45, 34, 23, 12, and 1. 
          Whoever gets to a number in that list first, is able to force a 
          win. Another example: goal = 20, start = 0, cmin = 5, cmax = 7.
          Looking at this case, for every bad cmax values, there are cmin 
          good values.  so starting at 20 we want to subtract 7+1 or 8 to 
          get to our first force win option 12.  Then since we have more 
          than 1 for our cmin, we need to get a range of 5 numbers for 
          the force win option. So we get 12, 11, 10, 9, and 8 as winning
          options.  Then subtract cmax again and we get 1 as a winning 
          option aswell.


TODO:
	  1. Add a player vs player element
	  2. Work on the recursive get win options function (see bugs)

Bugs:
	  1. Get win options doesnt always get all of them (misses bottom range
 	     sometimes)
	  2. Random error in recursive get win options - really rare -not sure
 	     whats going on

Added element:
	  I made the game so that cmin could be another value other than 1.
	  This also changes the winning options function.  